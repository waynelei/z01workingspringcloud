0. mkdir -p ~/mount_dockers/mysql

1. mvn clean install

2. docker-compose up

3. testing 

	a. http://localhost
	
	b. http://localhost/hello
	
	c. http://localhost/hellokitty

        d. curl -H "Content-Type: application/json" -X POST -d '{"name":"xyz","password":"password"}' http://localhost/user

           curl -H "Content-Type: application/json" -X POST -d '{"name":"abc","password":"passwordabc"}' http://localhost/user

           http://localhost/user

           http://localhost/user/abc

           curl -H "Content-Type: application/json" -X POST -d '{"name":"ABC","password":"password"}' http://localhost/user/login
       
           curl -H "Content-Type: application/json" -X POST -d '{"name":"abc","password":"passwordabc"}' http://localhost/user/login

           curl -H "Content-Type: application/json" -X DELETE  http://localhost/user/

        e. mysql -h 127.0.0.1 -P 3306 -udbuser  -pdbpass
        
4. reset database 

  rm -rf ~/mount_dockers/mysql/ 

  mkdir -p ~/mount_dockers/mysql

  docker-compose up -d




##node: don't use 3306 in your local mysql server, or it will conflict with docker mysql.