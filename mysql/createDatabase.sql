grant all privileges on *.* to 'root'@'%' identified by '1';
grant all privileges on *.* to 'root'@'localhost' identified by '1';
CREATE DATABASE leap;
CREATE USER 'dbuser'@'localhost' IDENTIFIED BY 'dbpass';
GRANT ALL ON leap.* TO 'dbuser'@'localhost';
CREATE USER 'dbuser'@'%' IDENTIFIED BY 'dbpass';
GRANT ALL ON leap.* TO 'dbuser'@'%';
