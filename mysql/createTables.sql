DROP TABLE IF EXISTS `leap`.`user`;

CREATE TABLE `leap`.`user` (
 `id` INT NOT NULL AUTO_INCREMENT,
 `name` VARCHAR(45) NOT NULL,
 `password` VARCHAR(255) NULL,
 PRIMARY KEY (`id`));
