/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author skoal
 */
public interface UserRepository extends CrudRepository<User, Integer> {
    List<User> findByName(String name);
}
