package com.example;

import java.io.UnsupportedEncodingException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.PathVariable;

@SpringBootApplication
@RestController
@EnableAutoConfiguration
@EnableDiscoveryClient
public class Z01workingSpringCloudLoginApplication {

    @Autowired
    UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(Z01workingSpringCloudLoginApplication.class, args);
    }

    @RequestMapping("/hellokitty")
    String home() {
        return "Hello Kitty!";
    }

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public ResponseEntity<User> update(@RequestBody User user) {
        System.out.println(user.getName());
        System.out.println(user.getPassword());

        String encryptedPassword = encryptString(user.getPassword());

        user.setPassword(encryptedPassword);

        userRepository.save(user);
        return new ResponseEntity<>(user, HttpStatus.OK);

    }

    @RequestMapping(value = "/user/login", method = RequestMethod.POST)
    public ResponseEntity login(@RequestBody User user) {
        System.out.println(user.getName());
        System.out.println(user.getPassword());
        
        String name = user.getName();
        String password =encryptString(user.getPassword());
        
        List<User> users = userRepository.findByName(name);
        boolean result;
        if ( !users.isEmpty() && users.get(0).getPassword().equals(password)){
            result = true;
        }else {
            result = false;
        }
        return new ResponseEntity<>(result, HttpStatus.OK);

    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public ResponseEntity<List<User>> loadUsers() {
        List<User> users = (ArrayList<User>) userRepository.findAll();

        return new ResponseEntity<>(users, HttpStatus.OK);

    }

    @RequestMapping(value = "/user/{name}", method = RequestMethod.GET)
    public User findByName(@PathVariable String name) {
        List<User> users = userRepository.findByName(name);
        User user = null;
        if (!users.isEmpty()) {
            user = users.get(0);
        }

        return user;

    }

    @RequestMapping(value = "/user", method = RequestMethod.DELETE)
    public ResponseEntity delete() {

        userRepository.deleteAll();
        return new ResponseEntity<>( HttpStatus.OK);

    }    
    
    
    private String encryptString(String str) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(str.getBytes("UTF-8"));

            StringBuilder hexString = new StringBuilder();
            for (int i : hash) {
                hexString.append(Integer.toHexString(0XFF & i));
            }
            str = hexString.toString();

        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            //Logger.getLogger(Z01workingSpringCloudLoginApplication.class.getName()).log(Level.SEVERE, null, ex);

        }
        return str;
    }
}
