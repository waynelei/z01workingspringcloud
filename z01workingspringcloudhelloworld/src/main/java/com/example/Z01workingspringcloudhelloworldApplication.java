package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableAutoConfiguration
@EnableDiscoveryClient
public class Z01workingspringcloudhelloworldApplication {

    @RequestMapping("/hello")
    String home() {
        return "Hello World!!!!!";
    }

    @RequestMapping("/")
    String root() {
        return "Hello root!!!!!";
    }

    public static void main(String[] args) {
        SpringApplication.run(Z01workingspringcloudhelloworldApplication.class, args);
    }
}
