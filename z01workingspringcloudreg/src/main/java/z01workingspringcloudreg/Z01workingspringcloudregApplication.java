package z01workingspringcloudreg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class Z01workingspringcloudregApplication {

	public static void main(String[] args) {
		SpringApplication.run(Z01workingspringcloudregApplication.class, args);
	}
}
